<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>ZSH FZF completion</title>
  <link rel="stylesheet" type="text/css" href="../general.css" />
  <link rel="stylesheet" type="text/css" href="../screen.css" />
  <link rel="stylesheet" type="text/css" href="../print.css" />
  <!--[if lt IE 9]>
      <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
</head>

<body dir="auto">
  <h1 id="zsh-easter-eggs">
    ZSH FZF completion
  </h1>
  <p>
    The group of command line users is divided to two:
  </p>
  <ul>
    <li>Those who use ZSH.
    </li>
    <li>Those who don't.
    </li>
  </ul>
  <p>
    If you belong to the 1st group, I'd like to share with you a few cool and
    very unknown features of ZSH. You’ll be amazed how comfortable your command
    line UI can get better.
  </p>
  <p>
    Note: This article gathers some excerpts from my <a href="https://github.com/doronbehar/dotfiles"><code>dotfiles</code>
      repository</a>. A list of the relevant files is available at the end of the article.
  </p>
  <h2 id="about-fzf">
    About <a href="https://github.com/junegunn/fzf">FZF</a>
  </h2>
  <blockquote>
    <p>
      Do you know <code>fzf</code>? The fuzzy file finder? Oh no! You don't?!
      Well it's about time!
    </p>
  </blockquote>
  <p>
    If you really haven't heard about <code>fzf</code> yet, you have no idea how thrilled
    you will be when you'll get familiar with it.
  </p>
  <blockquote>
    <p>
      fzf is a general-purpose command-line fuzzy finder. It’s an interactive
      Unix filter for command-line that can be used with any list; files, command
      history, processes, hostnames, bookmarks, git commits, etc.
    </p>
  </blockquote>
  <p>
    It is written in <a href="https://golang.org">go</a> which is a statically
    typed, compiled programming language designed by Google (It means it's very
    fast).
  </p>
  <p>
    Basically, <code>fzf</code> is designed to receive a list of strings
    (separated by <code>\n</code> or <code>\0</code>) and allow you to
    interactively choose a specific string from the list by typing parts of it.
    When you are satisfied with the choice, you press <kbd>Enter</kbd> and the
    result is printed. You can use this result to perform another action. You can
    use <code>fzf</code> in a shell function to <code>cd</code> into a specific
    directory for example.
  </p>
  <p>
    Here is an ASCIInema screen cast of <code>fzf</code> running with an input
    of the command <code>find /usr/share/</code>:
  </p><a href="https://asciinema.org/a/215320"><img src="../_shared/basic-fzf-usage.gif"
      alt="basic usage of FZF" width="100%" /></a>
  <p>
    I like to think of <code>fzf</code> as a general purpose search engine but
    very fast and embeddable. In this part of the article I'll explain how FZF
    can be used to magnificently improve your command line usage. I've gathered
    the knowledge needed to create the shell functions presented in this article
    from <a href="https://github.com/junegunn/fzf/wiki">FZF's WiKi</a>.
  </p>
  <p>
    Before we’ll begin, you should be familiar with ZSH’s Zle - the ZSH Line
    Editor which is capable of being manipulated and configured so in interactive
    shells you’ll be able to edit your command easily before running it. The
    builtin <code>zle</code> can define so-called <em>Widgets</em> which can
    manipulate your currently edited command. These can be binded to certain keys
    in order to be invoked easily. These is how the <code>fzf</code> based
    shortcuts of this article will be defined. You can read more about the Zsh
    Line Editor in the official ZSH documentation: <a href="https://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html">
      https://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html</a>
  </p>
  <h3 id="built-in-zsh-scripts-from-fzf-repository">
    Built-in ZSH scripts from FZF repository
  </h3>
  <p>
    Depends on your Linux distribution, and the way you installed FZF, there are
    2 scripts for ZSH which are part of FZF which you can <code>source</code> and
    use them right out of the box:
  </p>
  <ul>
    <li>
      <a href="https://github.com/junegunn/fzf/blob/d4ed955aee08a1c2ceb64e562ab4a88bdc9af8f0/shell/completion.zsh"><code>completion.zsh</code></a>
      - Creates a single <code>fzf</code> based widget and binds it to <kbd>Tab</kbd>,
      it's behaviour is similar to a behaviour of a standard completion in ZSH.
    </li>
    <li>
      <a href="https://github.com/junegunn/fzf/blob/d4ed955aee08a1c2ceb64e562ab4a88bdc9af8f0/shell/key-bindings.zsh"><code>key-bindings.zsh</code></a>
      - Binds several keys to other widgets which feel less like an argument
      completion (for example inserting to the line editor a command from the
      history using <code>fzf</code>).
    </li>
  </ul>
  <p>
    To exemplify what best these scripts provide by themselves, Try to source
    them and complete an argument for the <code>kill</code> command. This should
    look like so:
  </p>
  <p>
    <a href="https://asciinema.org/a/215342"><img src="kill-command-fzf-completion.gif"
        alt="basic usage of FZF" width="100%" /></a>
  </p>
  <h2 id="how-to-programmatically-change-the-line-in-zle">How to Programmatically Change the Line in Zle</h2>
  <p>
    What we've seen above is an Zle widget that is defined in <code>/usr/share/fzf/completion.zsh</code>
    and it is configured to be triggered when <kbd>Tab</kbd> is pressed.
  </p>
  <p>
    Basically, as an example for a very simple Zle widget, try to run the
    following commands right in your shell:
  </p>
<div class="sourceCode" id="cb1">
  <pre class="sourceCode zsh"><code class="sourceCode zsh"><a class="sourceLine" id="cb1-1" title="1"><span class="fu">example-zle-widget()</span><span class="kw">{</span></a>
<a class="sourceLine" id="cb1-2" title="2">  <span class="co"># Adds to the built-in LBUFFER variable the word &quot;HEY&quot;</span></a>
<a class="sourceLine" id="cb1-3" title="3">  <span class="ot">LBUFFER=</span><span class="st">&quot;</span><span class="ot">${LBUFFER}</span><span class="st">HEY&quot;</span></a>
<a class="sourceLine" id="cb1-4" title="4"><span class="kw">}</span></a>
<a class="sourceLine" id="cb1-5" title="5"><span class="kw">zle</span> -N example-zle-widget</a>
<a class="sourceLine" id="cb1-6" title="6"><span class="kw">bindkey</span> <span class="st">&#39;^E&#39;</span> example-zle-widget</a></code></pre>
</div>
  <p>
    This creates a function that very simply manipulates the built-in variable
    <code>LBUFFER</code> with the addition of the word <code>"HEY"</code>.
    Afterwards, we tell Zle that this function is not just a normal function but
    that it is meant to manipulate our edit buffer. In order to be able to trigger
    this widget, we've binded it to <kbd>Ctrl-E</kbd>. Other variables like
    <code>LBUFFER</code>, such as <code>RBUFFER</code> and <code>CURSOR</code>
    can be changed in this widget functions and programmatically change the text
    and the position of the cursor on the line. This is thoroughly documented in <a
      href="http://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html#User_002dDefined-Widgets">ZSH's
      Line editor documentation</a>.
  </p>
  <p>
    You can put any code and even run <code>fzf</code> right in these widgets'
    functions and afterwards manipulate <code>LBUFFER</code> accordingly.
  </p>
  <p>
  Creating a new Zle widget function for every command is expensive because in
  order to use them you'd need to bind a different key for every widget and you
  probably wouldn't want that. That's why we actually create a master Zle widget
  function that calls a different function depending on the first word in our
  <code>LBUFFER</code>.
  </p>
  <h2 id="fzf-completion-functions-implentation">FZF Completion Functions Implentation</h2>
  <p>
    It is rather complicated to fully understand everything that is written in
    <code>/usr/share/fzf/completion.zsh</code> and <code>/usr/share/fzf/key-bindings.zsh</code>.
    Additionally, I don't like the way the shell code there is structured. IMO
    it is very unmaintainable and overly complicated.
  </p>
  <p>
    That's why I've rewritten similar functions based on my understanding of the
    functions from upstream <code>completion.zsh</code> and <code>keybindings.zsh</code>.
    It's not important to fully understand the master widget function which is
    binded in my case to <kbd>Ctrl-f</kbd>. You really only need to understand how
    the master widget (<code>fzf-completion</code>) calls custom FZF completion functions:
  </p>
<div class="sourceCode" id="cb1"><pre class="sourceCode zsh"><code class="sourceCode zsh"><a class="sourceLine" id="cb1-1" title="1">        <span class="kw">if</span> <span class="kw">type</span> _fzf_complete_<span class="ot">${cmd}</span> <span class="kw">&gt;</span> /dev/null; <span class="kw">then</span></a>
<a class="sourceLine" id="cb1-2" title="2">            _fzf_complete_<span class="ot">${cmd}</span> <span class="st">&quot;</span><span class="ot">${lbuf}</span><span class="st">&quot;</span> <span class="st">&quot;</span><span class="ot">${prefix}</span><span class="st">&quot;</span></a>
<a class="sourceLine" id="cb1-3" title="3">        <span class="kw">else</span></a>
<a class="sourceLine" id="cb1-4" title="4">            _fzf_complete_path <span class="st">&quot;</span><span class="ot">${lbuf}</span><span class="st">&quot;</span> <span class="st">&quot;</span><span class="ot">${prefix}</span><span class="st">&quot;</span> files</a>
<a class="sourceLine" id="cb1-5" title="5">        <span class="kw">fi</span></a></code></pre></div>
  <p>
  This part of the <code>fzf-completion</code> widget taken from <a href="https://github.com/doronbehar/dotfiles/blob/d20976b70837e7f231000ff0e7437361e3b6d572/.zsh-fzf-completions#L69">here</a>
  checks if there's a special function we can call that will update our line
  buffer using FZF. If there isn't, it runs the fallback <code>_fzf_complete_path</code>
  (with the additional argument <code>files</code> that tells it to complete
  files as opposed to <code>directories</code>). The first two arguments <code>"${lbuf}"</code>
  and <code>"${prefix}"</code> tell this custom FZF completion function additional information about the words before the cursor:
  </p>
  <ul>
    <li><code>"${lbuf}"</code> is the text in the left side of the cursor, not
      including the prefix of the last unfinished word.</li>
    <li><code>"${prefix}"</code> is the text of the last word which the cursor is on.</li>
  </ul>
  <p>
    These variables are calculated prior to these calls, don't worry about it too much.
  </p>
  <h3 id="custom-fzf-completion-function-example">Custom FZF Completion Function Example</h3>
  <p>
    When implementing the various custom fzf completion functions, I chose to pass
    the <code>"${prefix}"</code> argument to <code>fzf</code>'s <code>--query</code>
    option, this seems the most reasonable thing to do IMO. Usually these functions start the same, like in <code>_fzf_complete_man</code>:
  </p>
<div class="sourceCode" id="cb1"><pre class="sourceCode zsh"><code class="sourceCode zsh"><a class="sourceLine" id="cb1-1" title="1"><span class="fu">_fzf_complete_man()</span><span class="kw">{</span></a>
<a class="sourceLine" id="cb1-2" title="2">    <span class="kw">local</span> <span class="ot">lbuf=</span><span class="st">&quot;</span><span class="ot">$1</span><span class="st">&quot;</span></a>
<a class="sourceLine" id="cb1-3" title="3">    <span class="kw">local</span> <span class="ot">prefix=</span><span class="st">&quot;</span><span class="ot">$2</span><span class="st">&quot;</span></a>
<a class="sourceLine" id="cb1-4" title="4">    <span class="kw">local</span> <span class="ot">name</span> <span class="ot">section</span> <span class="ot">dash</span> <span class="ot">description</span></a>
<a class="sourceLine" id="cb1-5" title="5">    <span class="kw">local</span> <span class="ot">matches=($(</span><span class="kw">man</span> -k <span class="kw">.</span> <span class="kw">|</span> fzf -m <span class="kw">|</span> <span class="kw">while</span> <span class="kw">read</span> -r <span class="ot">name</span> <span class="ot">section</span> <span class="ot">dash</span> <span class="ot">description</span>; <span class="kw">do</span></a>
<a class="sourceLine" id="cb1-6" title="6">        <span class="kw">echo</span> <span class="st">&quot;</span><span class="ot">$name</span><span class="st">.${</span><span class="ot">${section#</span>\(<span class="ot">}</span><span class="st">%\)}&quot;</span></a>
<a class="sourceLine" id="cb1-7" title="7">    <span class="kw">done</span><span class="ot">))</span></a>
<a class="sourceLine" id="cb1-8" title="8">    [ -z <span class="st">&quot;</span><span class="ot">$matches</span><span class="st">&quot;</span> ] <span class="kw">&amp;&amp;</span> <span class="kw">return</span> 1</a>
<a class="sourceLine" id="cb1-9" title="9">    <span class="ot">LBUFFER=</span><span class="st">&quot;</span><span class="ot">$LBUFFER${matches[@]}</span><span class="st">&quot;</span></a>
<a class="sourceLine" id="cb1-10" title="10"><span class="kw">}</span></a></code></pre></div>
  <p>
    It gets optional matches by running <code>fzf</code> in a subshell and inside
    it, we parse the results using standard shell methods like <code>while read</code> and variable substitution.
  </p>
  <p>
    In all of my shell functions, I tried to use mostly shell built-ins as much as
    possible and avoid using external commands such as <code>awk</code> and <code>sed</code>
    which usually very useful and comfortable but often prove to be slower then
    shell only code. This is where my approach is different then the one taken
    upstream where there they use fifo and all kinds of overlly complicated
    methods.
  </p>
  <h2 id="summarize">Summarize</h2>
  <p>
    All of the relevant shell functions are located in the following files:
  </p>
  <ul>
    <li>
      <a href="https://github.com/doronbehar/dotfiles/blob/5a24162962c0ac35889b21428281a8ec98d5f43a/.zsh-fzf-completions"><code>~/.zsh-fzf-completions</code></a>
      - Defines the basic Zle widgets and helper functions for completing arguments
      in the editor.
    </li>
    <li>
      <a href="https://github.com/doronbehar/dotfiles/blob/5a24162962c0ac35889b21428281a8ec98d5f43a/.zsh-fzf-command-completions"><code>~/.zsh-fzf-command-completions</code></a>
      - Defines special FZF based completion functions for commands that accept
      special arguments like <code>kill</code> and <code>ssh</code>.
    </li>
  </ul>
  <p>
  I've chosen to link here the latest versions (master branch) of these files
  here in the summary. I hope I won't make too many changes to the structure of
  my code so these links will become <code>404</code>. If I did and I forgot to update this document, please notify my in <a href="https://gitlab.com/doronbehar/doronbehar.gitlab.io/issues">the issue tracker of this website</a> :)
  </p>
</body>

</html>
