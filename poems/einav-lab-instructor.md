לכל אחד יש מעבדה,
שלא נותנת לו מנוח
לכל ניסוי ישנה מדידה,
שמוציאה לו את הכוח
בוא נאמר שבשבילי זה לא נורא,
בוא נאמר שזה בזכות המדריכה
בוא נאמר שזו לא הוצעת דיבה,
בוא נאמר שלשלי קוראים עינב

עינב, עינב
תראי לי איך לכתוב את התכנה
איך אני מחשב ת'רווח בר סמך
ומה זה R sqrt לא למדתי אף פעם
עינב, עינב,
עשינו כבר ת'דוח ואת הגרף
ה-errorbar קטן ונהדר
הפונקציה מתאימה מספיק לשנינו,

בוא נאמר שעינב עונה לכל מה שתשאל,
בוא נאמר שהיא טובה אליך גם אם תאחר,
היא כמו מלכה שעוזרת לכל מי שמבקש
צריך לומר תודה

עינב, עינב
